<?php
/**
 * Created by PhpStorm.
 * User: dad
 * Date: 12.09.18
 * Time: 1:55
 */
session_start();
require 'autoloader.php';
spl_autoload_register('classesAutoloader');
$response = new miscellaneous();
$login = new login();
if ($_POST['language']) $response -> languageSelect($_POST);
if ($_POST['write']) $response -> checkFields($_POST);
if ($_POST['users_select_form']) $response -> checkFields($_POST);
if ($_GET['q']) $response -> activateAccount($_GET);
if ($_POST['form']) switch ($_POST['form']) {
    case 'signin':
        $response -> checkLogin($_POST);
        break;
    case 'signup':
        $response -> checkFields($_POST);
        break;
    case 1:
        echo $login -> signupForm();
        break;
    case 2:
        echo $login -> loginForm();
        break;
    case 3:
        unset($_SESSION['profile']);
        $response -> goToMainPage();
        break;
    case 4:
        setcookie('edit', true, ( time() + 86400 ));
        $response -> goToMainPage();
        break;
    case 5:
        setcookie('edit', false, ( time() + 86400 ));
        $response -> goToMainPage();
        break;
    case 6:
        setcookie('messages_new', true, ( time() + 86400 ));
        $response -> goToMainPage();
        break;
    case 7:
        setcookie('messages_new', false, ( time() + 86400 ));
        $response -> goToMainPage();
        break;
    default:
        echo $login -> loginForm();
        break;
}
