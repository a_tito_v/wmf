<?php
/**
 * Created by PhpStorm.
 * User: dad
 * Date: 28.08.18
 * Time: 23:36
 *
 * @param $class
 */
function classesAutoloader($class)
{

    $filename = 'classes/'.$class.'.php';
    if (file_exists($filename)) include $filename; else echo "Class $class not found.";
}