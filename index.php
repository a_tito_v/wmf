<?php
/**
 * Created by PhpStorm.
 * User: dad
 * Date: 28.08.18
 * Time: 21:31
 */
session_start();
require 'autoloader.php';
spl_autoload_register('classesAutoloader');
$html = new html();
echo $html -> beforeBody() ?>
<div class="container" id="content">
    <?=$_SESSION['profile'] ? ( new language() ) -> languageSelector() : ''?>
    <?=$_SESSION['profile'] ? ( new profile() ) -> profileForm() : ( new login() ) -> loginForm()?>
</div>
<?=$html -> afterBody()?>
