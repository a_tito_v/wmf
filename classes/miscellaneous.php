<?php

/**
 * Created by PhpStorm.
 * User: dad
 * Date: 12.09.18
 * Time: 1:30
 */
class miscellaneous extends base
{

    /**
     * @param $post
     */
    public function languageSelect($post)
    {

        setcookie('language', $post['language'], ( time() + 86400 ));
        $this -> goToMainPage();

    }

    public function goToMainPage()
    {

        header('Location:'.( $this -> domain ).( $this -> path ));

    }

    /**
     * @param $post
     */
    public function checkLogin($post)
    {

        if (!empty($post)) {
            $loginModified = $this -> hs($post['login']);
            $passwordModified = $this -> hs($post['password']);
            $empty_fields = '';
            if (empty($loginModified)) $empty_fields .= 'login,';
            if (empty($passwordModified)) $empty_fields .= 'password';
            setcookie('empty_fields', $empty_fields, ( time() + 86400 ));
            $query = "select * from users where login='$loginModified' and password='".md5($passwordModified)."' and activate='1'";
            $result = $this -> dbQuery($query);
            if (count($result[0]) != 0) {
                session_start();
                $_SESSION['profile'] = $result[0][0];
            }
        }
        $this -> goToMainPage();

    }

    /**
     * @param $p
     *
     * @return string
     */
    public function hs($p)
    {

        return htmlspecialchars(stripslashes($p));
    }

    /**
     * @param $post
     */
    public function checkFields($post)
    {

        if (!empty($post)) {
            $modifiedLogin = $this -> hs($post['login']);
            $modifiedEmail = $this -> hs($post['email']);
            $modifiedFirstname = $this -> hs($post['firstname']);
            $modifiedLastname = $this -> hs($post['lastname']);
            $modifiedDob = $this -> hs($post['dob']);
            $modifiedPassword = $this -> hs($post['password']);
            $empty_fields = '';
            $status = '';
            if (empty($modifiedLogin)) $empty_fields .= 'login,';
            if (empty($modifiedFirstname)) $empty_fields .= 'firstname,';
            if (empty($modifiedLastname)) $empty_fields .= 'lastname,';
            if (empty($modifiedDob)) $empty_fields .= 'dob,';
            if (empty($modifiedPassword)) $empty_fields .= 'password,';
            if (empty($modifiedEmail)) $empty_fields .= 'email,'; elseif (!preg_match("/^[0-9a-z_\.]+@[0-9a-z_^\.]+\.[a-z]{2,6}$/i", $modifiedEmail)) $status = 'incorrect email';
            setcookie('empty_fields', $empty_fields, ( time() + 86400 ));
            setcookie('status', $status, ( time() + 86400 ));
            if ($post['users_select_form'] == '1') {
                $uploadDir = $_SERVER['DOCUMENT_ROOT'].$this -> path.'files/';
                $fileName = basename($_FILES['userfile']['name']);
                $uploadFile = $uploadDir.$post['id'].'_'.$fileName;
                $pathInfo = pathinfo($uploadFile);
                $extension = $pathInfo['extension'];
                $allowedExtension = [
                        'gif',
                        'jpg',
                        'png'];
                if (in_array($extension, $allowedExtension) and $_FILES['userfile']['size'] < $post['MAX_FILE_SIZE']) $allowed = true; else $allowed = false;
                if ($allowed ? move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadFile) : false) setcookie('upload', 'ok'); else setcookie('upload', 'error');
                $this -> dbQuery("insert into messages(id,date,user,to_user,message,image) values('','".date("Y-m-d")."','".$post['id']."','".$post['select']."','".htmlspecialchars(stripslashes($post['text']))."','".$fileName."')");
            }
            if ($post['write'] == 'yes') {
                $this -> dbQuery("update users set login='$modifiedLogin',".( ( $modifiedPassword == '' ) ? '' : "password='".md5($modifiedPassword)."'," )."email='$modifiedEmail',firstname='$modifiedFirstname',lastname='$modifiedLastname',dob='$modifiedDob' where id=".$post['id']);
            }
            if ($post['form'] == 'signup' and $empty_fields == '' and $status == '') {
                $mark = md5($modifiedLogin.time());
                setcookie('activate', $mark, ( time() + 86400 ));
                $uploadDir = $_SERVER['DOCUMENT_ROOT'].$this -> path.'files/';
                $uploadFile = $uploadDir.$modifiedLogin.'_'.basename($_FILES['userfile']['name']);
                $pathInfo = pathinfo($uploadFile);
                $extension = $pathInfo['extension'];
                $allowedExtension = [
                        'gif',
                        'jpg',
                        'png'];
                if (in_array($extension, $allowedExtension) and $_FILES['userfile']['size'] < $post['MAX_FILE_SIZE']) $allowed = true; else $allowed = false;
                if ($allowed ? move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadFile) : false) {
                    $this -> dbQuery("insert into users(id,login,password,email,firstname,lastname,dob,activate) values('','$modifiedLogin','".md5($modifiedPassword)."','$modifiedEmail','$modifiedFirstname','$modifiedLastname','$modifiedDob','$mark')");
                    //mail($modifiedEmail, 'Activating', '<a href="'.'http://'.$_SERVER[SERVER_NAME].$_SERVER[REQUEST_URI].'response.php?q='.$mark).'">Activate your account</a>';
                    setcookie('upload', 'ok');
                } else setcookie('upload', 'error');
            }
        }
        $this -> goToMainPage();
    }

    /**
     * @param $get
     */
    public function activateAccount($get)
    {

        $hash = $this -> hs($get['q']);
        $result = $this -> dbQuery("select activate from users where activate='$hash'");
        if ($result[0][0] == $hash) {
            $this -> dbQuery("update users set activate='1' where activate='$hash'");
            setcookie('activate', '', time() + 1);
        }
        $this -> goToMainPage();
    }
}
