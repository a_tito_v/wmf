<?php

/**
 * Created by PhpStorm.
 * User: dad
 * Date: 29.08.18
 * Time: 0:45
 */
class language extends base
{

    /**
     * @param $language
     * @param $text
     *
     * @return mixed
     */
    public function I18N($language, $text)
    {

        //english
        $m['en']['headsignin'] = 'Please sign in ';
        $m['en']['login'] = 'Enter your login';
        $m['en']['password'] = 'Enter your password';
        $m['en']['remember-me'] = ' Remember me';
        $m['en']['signin'] = 'Sign in';
        $m['en']['headsignup'] = 'Please sign up ';
        $m['en']['createlogin'] = 'Create your login';
        $m['en']['email'] = 'Enter your email address';
        $m['en']['firstname'] = 'Enter your first name';
        $m['en']['lastname'] = 'Enter your last name';
        $m['en']['dob'] = 'Enter your date of birth';
        $m['en']['createpassword'] = 'Create your password';
        $m['en']['signup'] = 'Sign up';
        $m['en']['orsignup'] = 'or sign up';
        $m['en']['orsignin'] = 'or sign in';
        $m['en']['fio'] = 'Personal data';
        $m['en']['login'] = 'Login';
        $m['en']['etc'] = 'Miscellaneous data';
        $m['en']['panel'] = 'Control panel';
        $m['en']['edit'] = 'Edit your profile';
        $m['en']['write'] = 'Submit';
        $m['en']['exit'] = 'Exit';
        $m['en']['warn'] = 'Message in your email:<br>';
        $m['en']['activ'] = 'Please, activate your account!';
        $m['en']['image'] = 'Select your image';
        $m['en']['messages'] = 'Messages';
        $m['en']['messages_date'] = 'Date';
        $m['en']['messages_from'] = 'From';
        $m['en']['messages_to'] = 'To';
        $m['en']['messages_message'] = 'Message';
        $m['en']['messages_image'] = 'Image';
        $m['en']['messages_new'] = 'New message';
        $m['en']['messages_new_send'] = 'Submit';
        //русский
        $m['ru']['headsignin'] = 'Вход ';
        $m['ru']['login'] = 'Введите ваш логин';
        $m['ru']['password'] = 'Введите ваш пароль';
        $m['ru']['remember-me'] = ' Запомнить меня';
        $m['ru']['signin'] = 'Войти';
        $m['ru']['headsignup'] = 'Регистрация ';
        $m['ru']['createlogin'] = 'Придумайте себе логин';
        $m['ru']['email'] = 'Введите адрес вашей электронной почты';
        $m['ru']['firstname'] = 'Введите ваше имя';
        $m['ru']['lastname'] = 'Введите вашу фамилию';
        $m['ru']['dob'] = 'Введите дату вашего рождения';
        $m['ru']['createpassword'] = 'Придумайте себе пароль';
        $m['ru']['signup'] = 'Зарегистрироваться';
        $m['ru']['orsignup'] = 'или регистрация';
        $m['ru']['orsignin'] = 'или войти';
        $m['ru']['fio'] = 'Личные данные';
        $m['ru']['login'] = 'Логин';
        $m['ru']['etc'] = 'Прочее';
        $m['ru']['panel'] = 'Панель управления';
        $m['ru']['edit'] = 'Редактировать профиль';
        $m['ru']['write'] = 'Записать';
        $m['ru']['exit'] = 'Выйти';
        $m['ru']['warn'] = 'Сообщение в вашем email:<br>';
        $m['ru']['activ'] = 'Пожалуйста, активируйте свой аккаунт!';
        $m['ru']['image'] = 'Выберите свою картинку';
        $m['ru']['messages'] = 'Сообщения';
        $m['ru']['messages_date'] = 'Дата';
        $m['ru']['messages_from'] = 'От кого';
        $m['ru']['messages_to'] = 'Кому';
        $m['ru']['messages_message'] = 'Сообщение';
        $m['ru']['messages_image'] = 'Изображение';
        $m['ru']['messages_new'] = 'Новое сообщение';
        $m['ru']['messages_new_send'] = 'Отправить';

        return $m[$language][$text];
    }

    /**
     * @return string
     */
    public function languageSelector()
    {

        return '
                <div id="language">
                    <form class="form-signin pull-right form-horizontal" method="post" action="response.php">
                        <select class="form-control" onChange="this.form.submit()" name="language">
                            <option '.( $this -> lang == 'en' ? 'selected' : '' ).' value="en">English</option>
                            <option '.( $this -> lang == 'ru' ? 'selected' : '' ).' value="ru">Русский</option>
                        </select>
                    </form>
                </div>';
    }

}