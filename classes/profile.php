<?php

/**
 * Created by PhpStorm.
 * User: dad
 * Date: 29.08.18
 * Time: 1:00
 */
class profile extends base
{

    /**
     * @return string
     */
    public function profileForm()
    {

        $table = '';
        $select = '';
        $lng = new language();
        $userID = $_SESSION['profile'];
        $user = $this -> getUserInfo()[0];
        $otherUsers = $this -> getOtherUsers($userID);
        $messages = $this -> getMessages($userID);
        foreach ($otherUsers as $otherUser) {
            $select = $select.'<option value="'.$otherUser[0].'">'.$otherUser[4].' '.$otherUser[5].'</option>';
            $usersIDs[$otherUser[0]] = $otherUser[4].' '.$otherUser[5];
        }
        foreach ($messages as $message) $table .= '<tr><td>'.$message[1].'</td><td>'.$usersIDs[$message[2]].'</td><td>'.$usersIDs[$message[3]].'</td><td>'.$message[4].'</td><td>'.$message[5].'</td></tr>';
        $edit = $_COOKIE['edit'];
        $messages_new = $_COOKIE['messages_new'];

        return '
		'.( $edit ? '<form method="post" action="response.php">' : '' ).'
			<div class="container">
				<div class="row row-offcanvas row-offcanvas-right">
					<div class="col-xs-12 col-sm-9">
						<div class="row">
							<div class="col-6 col-sm-6 col-lg-4">
								<h2>'.$lng -> I18N($this -> lang, 'fio').'</h2>
								<p>
									<input hidden value="'.$user[0].'" name="id">
									<input type="text" class="form-control"'.( $edit ? '' : ' disabled' ).' value="'.$user[4].'" placeholder="'.$lng -> I18N($this -> lang, 'firstname').'" name="firstname" title="'.$lng -> I18N($this -> lang, 'firstname').'">
									<input type="text" class="form-control"'.( $edit ? '' : ' disabled' ).' value="'.$user[5].'" placeholder="'.$lng -> I18N($this -> lang, 'lastname').'" name="lastname" title="'.$lng -> I18N($this -> lang, 'lastname').'">
									<input type="date" class="form-control"'.( $edit ? '' : ' disabled' ).' value="'.$user[6].'" placeholder="'.$lng -> I18N($this -> lang, 'dob').'" name="dob" title="'.$lng -> I18N($this -> lang, 'dob').'">
								</p>
							</div>
							<div class="col-6 col-sm-6 col-lg-4">
								<h2>'.$lng -> I18N($this -> lang, 'login').'</h2>
								<p>
									<input type="text" class="form-control"'.( $edit ? '' : ' disabled' ).' value="'.$user[1].'" placeholder="'.$lng -> I18N($this -> lang, 'createlogin').'" name="login" title="'.$lng -> I18N($this -> lang, 'createlogin').'">
									<input type="password" class="form-control"'.( $edit ? '' : ' disabled' ).' value="" placeholder="'.$lng -> I18N($this -> lang, 'createpassword').'" name="password" title="'.$lng -> I18N($this -> lang, 'createpassword').'">
								</p>
							</div>
							<div class="col-6 col-sm-6 col-lg-4">
								<h2>'.$lng -> I18N($this -> lang, 'etc').'</h2>
								<p>
									<input type="email" class="form-control"'.( $edit ? '' : ' disabled' ).' value="'.$user[3].'" placeholder="'.$lng -> I18N($this -> lang, 'email').'" name="email" title="'.$lng -> I18N($this -> lang, 'email').'">
								</p>
							</div>
						</div>
						<div class="col-md-12">
							<h2>'.$lng -> I18N($this -> lang, 'messages').'</h2>'.( $messages_new ? '<form enctype="multipart/form-data" name="select_form" class="form-inline" method="post" action="response.php">' : '' ).'
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<th>'.$lng -> I18N($this -> lang, 'messages_date').'</th>
										<th>'.$lng -> I18N($this -> lang, 'messages_from').'</th>
										<th>'.$lng -> I18N($this -> lang, 'messages_to').'</th>
										<th>'.$lng -> I18N($this -> lang, 'messages_message').'</th>
										<th>'.$lng -> I18N($this -> lang, 'messages_image').'</th>
									</tr>
								</thead>
								<tbody>'.( $messages_new ? '
                                    <input type="hidden" name="MAX_FILE_SIZE" value="300000" />
									<tr><td>'.date("d.m.Y").'</td>
									<td></td>
									<td><select name="select" class="btn btn-xs btn-default">'.$select.'</select></td>
									<td><textarea rows="5" cols="25" name="text"></textarea></td>
                                    <td><input type="file" class="form-control" placeholder="'.$lng -> I18N($this -> lang, 'image').' (gif,jpg,png <30000b) " name="userfile" title="'.$lng -> I18N($this -> lang, 'image').' (gif,jpg,png <30000b)">
									<input type="hidden" name="users_select_form" value="1">
									<input type="hidden" name="id" value="'.$userID.'"><br><br>
									<button type="submit" class="btn btn-success btn-xs btn-block" onclick="call(7)">'.$lng -> I18N($this -> lang, 'messages_new_send').'</button></td>
									</tr>'.$table.'</form>' : $table ).'
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
						<div class="well sidebar-nav">
							<ul class="nav">
								<li>'.$lng -> I18N($this -> lang, 'panel').'</li><br>
								<li>
									<button class="btn btn-xs btn-warning btn-block" type="submit" name="exit" value="exit" onclick="call(3)">'.$lng -> I18N($this -> lang, 'exit').'</button>
								</li>
								<br>
								<li>
									<button class="btn btn-xs btn-success btn-block" type="submit" name="messages_new" value="1" onclick="call(6)">'.$lng -> I18N($this -> lang, 'messages_new').'</button>
								</li>
								<br>
								<li>
									'.( !$edit ? '<button class="btn btn-xs btn-info btn-block" type="submit" name="edit" value="yes" onclick="call(4)">'.$lng -> I18N($this -> lang, 'edit').'</button>' : '' ).'
								</li>
								<li>
									'.( $edit ? '<button class="btn btn-xs btn-danger btn-block" type="submit" name="write" value="yes" onclick="call(5)">'.$lng -> I18N($this -> lang, 'write').'</button>' : '' ).'
								</li>
							</ul>
						</div>
					</div>
				</div>
				<hr>
			</div>
		</form>';
    }

    /**
     * @return array
     */
    private function getUserInfo()
    {

        return $this -> dbQuery('select * from users where id='.$_SESSION['profile']);
    }

    /**
     * @param $userID
     *
     * @return array
     */
    private function getOtherUsers($userID)
    {

        return $this -> dbQuery("select * from users where id<>$userID");
    }

    /**
     * @param $userID
     *
     * @return array
     */
    private function getMessages($userID)
    {

        return $this -> dbQuery("(select * from messages where user=$userID) union (select * from messages where to_user=$userID) order by id desc");
    }

}