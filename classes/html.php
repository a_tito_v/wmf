<?php

/**
 * Created by PhpStorm.
 * User: dad
 * Date: 28.08.18
 * Time: 23:55
 */
class html extends base
{

    /**
     * @return string
     */
    public function beforeBody()
    {

        return '
		<!doctype html>
		<html>
			<head>
				<title>test</title>
				<meta name="viewport" content="width=device-width">
				<meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
				<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/>
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" type="text/javascript"></script>
				<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
				<script>
					function call(number){
						$.ajax({
							type:"POST",
							url:"response.php",
							data:"form="+number,
							success:function(data){
								$("#content").html(data);}});}
				</script>
                <link href="css/style.css" rel="stylesheet"  type="text/css">
			</head>
		<body>';
    }

    /**
     * @return string
     */
    public function afterBody() { return '</body></html>'; }

}