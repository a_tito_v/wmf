<?php

/**
 * Created by PhpStorm.
 * User: dad
 * Date: 29.08.18
 * Time: 0:46
 */
class login extends base
{

    /**
     * login constructor.
     */
    public function loginForm()
    {

        $language = new language();
        $activate = ( strlen($_COOKIE['activate']) == 32 ) ? '<div class="alert alert-dismissable alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button><b>'.$language -> I18N($this -> lang, 'warn').'</b> <a href="'.'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'response.php?q='.$_COOKIE['activate'].'">'.$language -> I18N($this -> lang, 'activ').'</a></div>' : '';

        return $language -> languageSelector().'
            <form class="form-signin" method="post" action="response.php">'.$activate.'
                <h2 class="form-signin-heading">'.$language -> I18N($this -> lang, 'headsignin').'<a class="btn btn-success btn-xs navbar-right" onclick="call(1)">'.$language -> I18N($this -> lang, 'orsignup').'</a></h2>
                <input required type="text" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'login').'" name="login" title="'.$language -> I18N($this -> lang, 'login').'">
                <input required type="password" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'password').'" name="password" title="'.$language -> I18N($this -> lang, 'password').'">
                <label class="checkbox"><input type="checkbox" value="1" name="remember-me">'.$language -> I18N($this -> lang, 'remember-me').'</label>
                <button class="btn btn-lg btn-primary btn-block" type="submit" name="form" value="signin">'.$language -> I18N($this -> lang, 'signin').'</button>
            </form>';
    }

    /**
     * @return string
     */
    public function signupForm()
    {

        $language = new language();

        return $language -> languageSelector().'
            <form enctype="multipart/form-data" class="form-signin" method="post" action="response.php">
                <h2 class="form-signin-heading">'.$language -> I18N($this -> lang, 'headsignup').'<a class="btn btn-primary btn-xs navbar-right" onclick="call(2)">'.$language -> I18N($this -> lang, 'orsignin').'</a></h2>
                <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                <input required type="text" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'createlogin').'" name="login" title="'.$language -> I18N($this -> lang, 'createlogin').'">
                <input required type="email" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'email').'" name="email" title="'.$language -> I18N($this -> lang, 'email').'">
                <input required type="text" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'firstname').'" name="firstname" title="'.$language -> I18N($this -> lang, 'firstname').'">
                <input required type="text" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'lastname').'" name="lastname" title="'.$language -> I18N($this -> lang, 'lastname').'">
                <input required type="date" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'dob').'" name="dob" title="'.$language -> I18N($this -> lang, 'dob').'">
                <input required type="password" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'createpassword').'" name="password" title="'.$language -> I18N($this -> lang, 'createpassword').'">
                <input required type="file" class="form-control" placeholder="'.$language -> I18N($this -> lang, 'image').' (gif,jpg,png <30000b) " name="userfile" title="'.$language -> I18N($this -> lang, 'image').' (gif,jpg,png <30000b)">
                <button class="btn btn-lg btn-success btn-block" type="submit" name="form" value="signup">'.$language -> I18N($this -> lang, 'signup').'</button>
            </form>';
    }

}
