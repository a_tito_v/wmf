<?php

/**
 * Created by PhpStorm.
 * User: dad
 * Date: 28.08.18
 * Time: 23:40
 */
class base
{

    public $debug = true;
    public $lang;
    public $domain;
    public $path;

    public function __construct()
    {

        $this -> domain = 'http'.( $_SERVER['HTTPS'] == 'on' ? 's' : '' ).'://'.$_SERVER['SERVER_NAME'];
        $this -> path = '/wmf/';
        if (!isset($_COOKIE['language'])) $this -> lang = 'en'; else $this -> lang = $_COOKIE['language'];//
        /*mysql*/
        $host = 'localhost';
        $db = 'wmf';
        $user = 'root';
        $password = 'ramzes';
        $this -> link = mysqli_connect($host, $user, $password, $db);
    }

    /**
     * @param        $message
     * @param string $comment
     * @param string $method
     *
     * @return null
     */
    public function msg($message, $comment = '', $method = '')
    {

        if ($this -> debug) {
            echo '<div class="small">';
            echo $comment ? ( $method ? $method.'-->' : '' ).'<b>'.$comment.'==</b>' : '';
            if (is_array($message)) {
                $this -> prettyPrint($message, false);
            } else {
                echo $message.'<br>';
            }
            echo '</div>';
        }

        return null;
    }

    /**
     * @param      $in
     * @param bool $opened
     */
    public function prettyPrint($in, $opened = true)
    {

        if ($opened) {
            $opened = ' open';
        }
        if (is_object($in) or is_array($in)) {
            echo '<div>';
            echo '<details'.$opened.'>';
            echo '<summary>';
            echo ( is_object($in) ) ? 'Object {'.count((array)$in).'}' : 'Array ['.count($in).']';
            echo '</summary>';
            $this -> prettyPrintRec($in, $opened);
            echo '</details>';
            echo '</div>';
        }
    }

    /**
     * @param     $in
     * @param     $opened
     * @param int $margin
     */
    public function prettyPrintRec($in, $opened, $margin = 10)
    {

        if (!is_object($in) && !is_array($in)) {
            return;
        }
        $bgc = 'white';
        foreach ($in as $key => $value) {
            if (is_object($value) or is_array($value)) {
                echo '<details style="margin-left:'.$margin.'px" '.$opened.'>';
                echo '<summary>';
                echo ( is_object($value) ) ? $key.' {'.count((array)$value).'}' : $key.' ['.count($value).']';
                echo '</summary>';
                $this -> prettyPrintRec($value, $opened, $margin + 10);
                echo '</details>';
            } else {
                switch (gettype($value)) {
                    case 'string':
                        $bgc = 'navy';
                        break;
                    case 'integer':
                        $bgc = 'red';
                        break;
                }
                echo '<div style="margin-left:'.$margin.'px">'.$key.' : <span style="color:'.$bgc.'">'.htmlspecialchars($value).'</span> ('.gettype($value).')</div>';
            }
        }
    }

    /**
     * @param $query
     *
     * @return array
     */
    public function dbQuery($query)
    {

        $response = [];
        if ($this -> link -> multi_query("set names utf8; set sql_mode = ''; $query;")) {
            do {
                if ($t = $this -> link -> use_result()) {
                    while ($r = $t -> fetch_row()) $response[] = $r;
                    $t -> close();
                }
            } while (mysqli_more_results($this -> link) && $this -> link -> next_result());
        }

        return $response;
    }

}